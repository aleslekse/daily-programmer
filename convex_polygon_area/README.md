Convex polygons
===============

Tool that calculates area of polygon. If `-int true` switch is specified then calculate area of intersection between two polygons.

Running
-------

Can be ran in two modes, interactive mode and automatic.

Interactive mode: `./convex_polygon_area`. Application will guide you through parameter gathering and display area when completed.

Automatic mode: `./convex_polygon_area < input1.txt`

Switches
--------

`-int` If true then calculate area of intersection between two convex poylgons
