/*
*	http://www.reddit.com/r/dailyprogrammer/comments/29umz8/742014_challenge_169_hard_convex_polygon_area/
 */
package main

import (
	"flag"
	"fmt"
)

func readPolygon() (polygon, bool) {
	instr("Enter number of vertices")

	var n int
	_, err := fmt.Scanf("%d", &n)
	if err != nil {
		fmt.Println("Bad number of vertices")
		return nil, true
	}
	if n <= 2 {
		fmt.Println("Please enter more than two vertices")
		return nil, true
	}

	p := make(polygon, 0, n)
	for i := 0; i < n; i++ {
		instr("Please enter point %d", i+1)
		var x, y float64
		_, err := fmt.Scanf("%f,%f", &x, &y)
		if err != nil {
			fmt.Println("Bad point coordinates")
			return nil, true
		}
		p = append(p, point{x, y})
	}

	return p, false
}

func instr(msg string, args ...interface{}) {
	if IsStdinTerminal() {
		fmt.Printf(msg, args...)
		fmt.Println()
	}
}

func main() {
	var calcInt = flag.Bool("int", false, "Calculate intersection between two polygons")
	flag.Parse()

	if *calcInt {
		p1, err := readPolygon()
		if err {
			return
		}

		p2, err := readPolygon()
		if err {
			return
		}

		fmt.Println(p1.intersectArea(p2))
	} else {
		p, err := readPolygon()
		if !err {
			fmt.Printf("%f\n", p.area())
		}
	}

}
