package main

import (
	"math"
	"sort"
)

/*
*	Point class, defined by both coordinates
 */
type point struct {
	x, y float64
}

// Calculate dot product between two vectors
func (p point) dot(r *point) float64 {
	return p.x*r.x + p.y*r.y
}

// Calculated normalized dot product (between 0 and 1)
func (p point) dotN(r *point) float64 {
	return p.dot(r) / p.abs() / r.abs()
}

// Scales vector
func (p point) scale(f float64) *point {
	return &point{
		p.x * f,
		p.y * f,
	}
}

// Adds two vectors
func (p point) add(r *point) *point {
	return &point{r.x + p.x, r.y + p.y}
}

// Subtracts vector r from vector p
func (p point) sub(r *point) *point {
	return &point{p.x - r.x, p.y - r.y}
}

// Calculated length of vector
func (p point) abs() float64 {
	return math.Sqrt(p.x*p.x + p.y*p.y)
}

// Compares two vectors
func (a point) equals(b *point) bool {
	return a.x == b.x && a.y == b.y
}

// Calculated distance between two points
func (a point) dist(b *point) float64 {
	x, y := a.x-b.x, a.y-b.y

	x *= x
	y *= y

	return math.Sqrt(x + y)
}

/*
*	Util structure, used to sort points in regard to angle
 */
type angleIndex []struct {
	index int
	angle float64
}

func (l angleIndex) Len() int {
	return len(l)
}

func (l angleIndex) Swap(i, j int) {
	l[i], l[j] = l[j], l[i]
}

func (l angleIndex) Less(i, j int) bool {
	return l[i].angle < l[j].angle
}

/*
*	Line class, defined by two points
 */
type line struct {
	a, b point
}

// Calculate scale factors both lines would have to be scaled
// to intersect
func (l1 *line) intersectFactors(l2 *line) (float64, float64, bool) {
	m := matrix2{
		l1.b.x - l1.a.x, l2.a.x - l2.b.x, l2.a.x - l1.a.x,
		l1.b.y - l1.a.y, l2.a.y - l2.b.y, l2.a.y - l1.a.y,
	}

	return m.solve()
}

// Calculate intersection between two lines.
func (l1 *line) intersect(l2 *line) (in *point, err bool) {
	f1, f2, err := l1.intersectFactors(l2)

	if err || f1 < 0 || f1 > 1 || f2 < 0 || f2 > 1 {
		err = true
		return
	}

	in = l1.a.add(l1.b.sub(&l1.a).scale(f1))

	return
}

/*
*	Util class for solving 2 linear equations
*	with two unknowns
 */
type matrix2 struct {
	x1, x2, xc, y1, y2, yc float64
}

// solve equations
func (m *matrix2) solve() (xs, ys float64, err bool) {
	// swap lines
	if m.x1 == 0 {
		m.x1, m.y1 = m.y1, m.x1
		m.x2, m.y2 = m.y2, m.x2
		m.xc, m.yc = m.yc, m.xc
	}

	if m.x1 == 0 {
		err = true
		return
	}

	f := -m.y1 / m.x1
	m.y1 += m.x1 * f
	m.y2 += m.x2 * f
	m.yc += m.xc * f

	if m.y2 == 0 {
		err = true
		return
	}

	ys = m.yc / m.y2
	xs = (m.xc - m.x2*ys) / m.x1

	return
}

/*
*	Triangle class, defined by 3 points
 */
type triangle struct {
	a, b, c    point
	ab, ac, bc *line
}

// Accessor functions
func (t *triangle) getAb() *line {
	if t.ab == nil {
		t.ab = &line{t.a, t.b}
	}
	return t.ab
}
func (t *triangle) getAc() *line {
	if t.ac == nil {
		t.ac = &line{t.a, t.c}
	}
	return t.ac
}
func (t *triangle) getBc() *line {
	if t.bc == nil {
		t.bc = &line{t.b, t.c}
	}
	return t.bc
}
func (t *triangle) getPoints() [3]point {
	return [3]point{t.a, t.b, t.c}
}

// Returns true if point lies inside
func (t *triangle) isPointInside(pt *point) bool {
	cp := line{t.c, *pt}
	f1, f2, err := t.getAb().intersectFactors(&cp)

	return !err && f1 >= 0 && f1 <= 1 && f2 >= 1
}

// Calculate intersection between two triangles
func (t1 *triangle) intersect(t2 *triangle) polygon {
	pts1 := t1.getPoints()
	pts2 := t2.getPoints()
	inter := make([]point, 0, 6)
	lines1 := [3]*line{
		t1.getAb(), t1.getAc(), t1.getBc(),
	}
	lines2 := [3]*line{
		t2.getAb(), t2.getAc(), t2.getBc(),
	}

	// check if point is already contained inside slice
	contains := func(points []point, a *point) bool {
		for _, b := range points {
			if b.equals(a) {
				return true
			}
		}
		return false
	}

	// push points from array if they are located inside
	// triangle
	pushInsidePoints := func(t *triangle, pts *[3]point) {
		for _, p := range pts {
			if t.isPointInside(&p) && !contains(inter, &p) {
				inter = append(inter, p)
			}
		}
	}

	// pushing inside points to list of corners
	pushInsidePoints(t2, &pts1)
	pushInsidePoints(t1, &pts2)

	// push all intersections between triangle lines
	for _, l1 := range lines1 {
		for _, l2 := range lines2 {
			if p, err := l1.intersect(l2); err == false && !contains(inter, p) {
				inter = append(inter, *p)
			}
		}
	}

	n := len(inter)

	// determine corner's center point
	sx, sy := 0.0, 0.0
	for _, p := range inter {
		sx += p.x
		sy += p.y
	}
	cx, cy := sx/float64(n), sy/float64(n)

	byAngle := make(angleIndex, n)
	for i, p := range inter {
		byAngle[i].index = i
		byAngle[i].angle = math.Atan2(p.y-cy, p.x-cx)
	}
	sort.Sort(byAngle)

	ordered := make(polygon, 0, n)
	for _, r := range byAngle {
		ordered = append(ordered, inter[r.index])
	}

	return ordered
}

type polygon []point

func (p polygon) area() (area float64) {
	n := len(p)
	if n == 0 {
		return
	}

	a := p[0]
	for i := 1; i < n-1; i++ {
		b := p[i]
		c := p[i+1]

		ba := b.sub(&a)
		ca := c.sub(&a)

		angle := math.Pi - math.Acos(ba.dot(ca)/ba.abs()/ca.abs())
		if angle > 90 {
			angle = 180 - angle
		}
		area += (math.Sin(angle) * ba.abs() * ca.abs()) / 2
	}
	return
}

func (p polygon) intersectArea(r polygon) (area float64) {
	makeTrigs := func(p polygon) []triangle {
		n := len(p)
		slice := make([]triangle, 0, n-2)
		a := p[0]
		for i := 1; i < n-1; i++ {
			slice = append(slice, triangle{
				a, p[i], p[i+1], nil, nil, nil,
			})
		}
		return slice
	}

	trigs1 := makeTrigs(p)
	trigs2 := makeTrigs(r)

	for _, a := range trigs1 {
		for _, b := range trigs2 {
			area += a.intersect(&b).area()
		}
	}

	return
}
