package main

import (
	"code.google.com/p/plotinum/plot"
	"code.google.com/p/plotinum/plotter"
	"code.google.com/p/plotinum/plotutil"
	"fmt"
	"math"
	"math/cmplx"
)

func Dft(X []complex128, forward bool) []complex128 {
	size := len(X)
	N := float64(size)
	cos := make([]complex128, size)
	sign := -1.0
	invN := complex(1/N, 0)

	if !forward {
		sign = 1.0
	}

	for k := 0; k < size; k++ {
		for n, x := range X {
			kn := float64(n * k)
			cos[k] += x * complex(math.Cos(2*math.Pi*kn/N), sign*math.Sin(2*math.Pi*kn/N))
		}
		if !forward {
			cos[k] *= invN
		}
	}

	return cos
}

func fft(X []complex128, N, s, o int, forward bool) []complex128 {
	var R []complex128

	if N == 1 {
		R = make([]complex128, 1)
		R[0] = X[o]
		return R
	} else {
		E := fft(X, N/2, 2*s, o, forward)
		O := fft(X, N/2, 2*s, o+s, forward)
		fN := float64(N)
		s := 2.0
		if forward {
			s = -2.0
		}

		for k := 0; k < N/2; k += 1 {
			t := E[k]
			u := O[k]
			w := cmplx.Exp(complex(0.0, s*math.Pi*float64(k)/fN)) * u

			E[k] = t + w
			O[k] = t - w
		}

		R = make([]complex128, N)
		for k := 0; k < N/2; k++ {
			R[k] = E[k]
			R[k+N/2] = O[k]
		}
	}

	return R
}

func Fft(X []complex128, forward bool) []complex128 {
	R := fft(X, len(X), 1, 0, forward)

	if !forward {
		invN := complex(1/float64(len(X)), 0)

		for k := range R {
			R[k] *= invN
		}
	}

	return R
}

func makeComplex(X []float64) []complex128 {
	res := make([]complex128, len(X))
	for i, v := range X {
		res[i] = complex(v, 0.0)
	}
	return res
}

func makeReal(X []complex128) []float64 {
	res := make([]float64, len(X))
	for i, x := range X {
		res[i] = real(x)
	}
	return res
}

func makePoints(X []float64) plotter.XYs {
	pts := make(plotter.XYs, len(X))
	for i, x := range X {
		pts[i].X = float64(i)
		pts[i].Y = x
	}
	return pts
}

func filterCoefficients(X []complex128, delta float64) map[int]complex128 {
	filtered := make(map[int]complex128)

	for i, x := range X {
		if pow := cmplx.Abs(x); pow > delta {
			filtered[i] = x
		}

	}

	return filtered
}

func makeMap(X []complex128) map[int]complex128 {
	res := make(map[int]complex128)
	for i, x := range X {
		res[i] = x
	}

	return res
}

func makeSlice(X map[int]complex128) []complex128 {
	N := len(X)
	S := make([]complex128, 0, N)

	for k := 0; k < N; k++ {
		S = append(S, X[k])
	}

	return S
}

func formatSin(i int, size int, x complex128) string {
	var fun string
	var add float64
	var formatted string

	ampl := 2.0 * cmplx.Abs(x) / float64(size)
	phase := math.Atan2(imag(x), real(x))

	if phase < 0 {
		fun = "Sin"
		add = phase + math.Pi/2
	} else {
		fun = "Cos"
		add = phase
	}

	if math.Abs(ampl-1.0) > 1e-10 {
		formatted += fmt.Sprintf("%.1f*", ampl)
	}
	formatted += fun + "("
	if i > 1 {
		formatted += fmt.Sprintf("%d*", i)
	}
	formatted += "x"
	if math.Abs(add) > 1e-10 {
		formatted += fmt.Sprintf(" + %.1f", add)
	}
	formatted += ")"

	return formatted
}

func main() {
	//points := []float64{1.000000, 0.616019, -0.074742, -0.867709, -1.513756, -1.814072, -1.695685, -1.238285, -0.641981, -0.148568, 0.052986, -0.099981, -0.519991, -1.004504, -1.316210, -1.277204, -0.840320, -0.109751, 0.697148, 1.332076, 1.610114, 1.479484, 1.039674, 0.500934, 0.100986, 0.011428, 0.270337, 0.767317, 1.286847, 1.593006, 1.522570, 1.050172, 0.300089, -0.500000, -1.105360, -1.347092, -1.195502, -0.769329, -0.287350, 0.018736, -0.003863, -0.368315, -0.942240, -1.498921, -1.805718, -1.715243, -1.223769, -0.474092, 0.298324, 0.855015, 1.045127, 0.861789, 0.442361, 0.012549, -0.203743, -0.073667, 0.391081, 1.037403, 1.629420, 1.939760, 1.838000, 1.341801, 0.610829, -0.114220, -0.603767, -0.726857, -0.500000, -0.078413, 0.306847, 0.441288, 0.212848, -0.342305, -1.051947, -1.673286, -1.986306, -1.878657, -1.389067, -0.692377, -0.032016, 0.373796, 0.415623, 0.133682, -0.299863, -0.650208, -0.713739, -0.399757, 0.231814, 0.991509, 1.632070, 1.942987, 1.831075, 1.355754, 0.705338, 0.123579, -0.184921, -0.133598, 0.213573, 0.668583, 0.994522, 1.000000}

	points := make([]float64, 8192)
	for i := 0; i < len(points); i++ {
		x := float64(i) * 2.0 * math.Pi / float64(len(points))
		//points[i] = 3*math.Sin(2*x) + 0.5*math.Cos(10*x) + math.Cos(44*x+0.2*math.Pi)
		points[i] = 3*math.Sin(2*x) + 0.5*math.Cos(10*x)
	}

	//coefficients := Dft(makeComplex(points), true)
	coefficients := Fft(makeComplex(points), true)

	filtered := filterCoefficients(coefficients, 5)
	//invCoefficients := Dft(makeSlice(filtered), false)
	invCoefficients := Fft(makeSlice(filtered), false)

	fmt.Printf("Found: %d\n", len(filtered)/2)
	for i, x := range filtered {
		if i > len(points)/2 {
			break
		}
		fmt.Printf("- %s\n", formatSin(i, len(points), x))
	}

	p, _ := plot.New()
	p.Title.Text = fmt.Sprintf("Dft (%d/%d)", len(filtered), len(points))
	p.X.Label.Text = "Time"
	p.Y.Label.Text = "Value"
	plotutil.AddLinePoints(p,
		"Original", makePoints(points),
		"Dft", makePoints(makeReal(invCoefficients)))

	p.Save(16, 10, "chart.png")
}
