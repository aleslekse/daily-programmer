Bake Pi
=======

Running
-------

Run server: `./pi_bake -mode=m -p PORT`

Run worker(s): `./pi_bake -mode=s -host HOST -port PORT`

Get current digits of pi: `./pi_bake -mode=p -host HOST -port PORT`

Switches
--------

`-mode` Mode of operation: m=master, s=slave(worker), p=fetch pi  
`-host` Host on which master is running and clients connects to (default localhost)  
`-port` Port on which master is running and clients connects to (default 9000)  
