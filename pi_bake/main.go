package main

import (
	"bytes"
	"errors"
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"net/rpc"
	"os"
	"time"
)

var host string
var port int
var serverMode modeFlag

/*
*	Flag mode
 */
type modeFlag int

func (m *modeFlag) String() string {
	switch *m {
	case 0:
		return "m"
	case 1:
		return "s"
	case 2:
		return "p"
	}
	return ""
}

func (m *modeFlag) Set(value string) error {
	switch value {
	case "m":
		*m = 0

	case "s":
		*m = 1

	case "p":
		*m = 2

	default:
		return errors.New(fmt.Sprintf("unknown mode %s", value))
	}

	return nil
}

func (m *modeFlag) IsServer() bool {
	return *m == 0
}
func (m *modeFlag) IsClient() bool {
	return *m == 1
}
func (m *modeFlag) IsPi() bool {
	return *m == 2
}

type JobRequest struct {
	From, To int
	Id       int
	Started  int64
}

func (req *JobRequest) Duration() int64 {
	return time.Now().Unix() - req.Started
}

type JobResponse struct {
	Digits []byte
	Id     int
}

func NewJobResponse(req JobRequest) *JobResponse {
	rsp := new(JobResponse)
	rsp.Id = req.Id
	rsp.Digits = make([]byte, 0, req.To-req.From)

	return rsp
}

func (r *JobResponse) Add(digit byte) {
	r.Digits = append(r.Digits, digit)
}

/*
*	PiRpc
 */
type PiRpc struct {
	digits          map[int]byte
	jobs            map[int]*JobRequest
	lastDigit       int
	lastJobId       int
	lastReqDuration int64
}

func NewPiRpc() *PiRpc {
	rpc := new(PiRpc)
	rpc.digits = make(map[int]byte)
	rpc.jobs = make(map[int]*JobRequest)

	return rpc
}

func (s *PiRpc) RequestJob(dummy int, req *JobRequest) error {
	if s.lastReqDuration > 0 {
		for _, job := range s.jobs {
			if job.Duration() > s.lastReqDuration*2 {
				fmt.Printf("Rescheduling #%d\n", job.Id)
				*req = *job
				req.Started = time.Now().Unix()
				return nil
			}
		}
	}

	fmt.Printf("Scheduling #%d\n", s.lastJobId)

	from, to := s.lastDigit, s.lastDigit+10
	id := s.lastJobId

	req.From, req.To = from, to
	req.Id = id
	req.Started = time.Now().Unix()
	s.jobs[s.lastJobId] = req

	s.lastDigit += 10
	s.lastJobId++

	return nil
}

func (s *PiRpc) PublishResults(res *JobResponse, dummy *int) error {
	fmt.Printf("Got results for #%d\n", res.Id)

	job, ok := s.jobs[res.Id]

	if !ok {
		return nil
	}

	for index, digit := range res.Digits {
		s.digits[job.From+index] = digit
	}
	s.lastReqDuration = job.Duration()

	delete(s.jobs, res.Id)

	return nil
}

func (s *PiRpc) Pi(dummy *int, pi *string) error {
	var buffer bytes.Buffer

	n := len(s.digits)
	for i := 0; i < n; i++ {
		digit, ok := s.digits[i]
		if !ok {
			break
		}

		fmt.Fprintf(&buffer, "%X", digit)
	}

	*pi = buffer.String()

	return nil
}

func usage() {
	fmt.Fprintf(os.Stderr, "usage: pi_bake -mode m -port 9000\n")
	flag.PrintDefaults()
	os.Exit(2)
}

func init() {
	flag.IntVar(&port, "port", 9000, "port")
	flag.StringVar(&host, "host", "localhost", "host")
	flag.Var(&serverMode, "mode", "mode, either m (master), s (slave) or p (pi)")
}

func startServer() {
	rpc.Register(NewPiRpc())
	rpc.HandleHTTP()
	l, e := net.Listen("tcp", fmt.Sprintf("%s:%d", host, port))
	if e != nil {
		log.Fatal("listen error:", e)
	}
	http.Serve(l, nil)
}

func startClient() {
	client, err := rpc.DialHTTP("tcp", fmt.Sprintf("%s:%d", host, port))
	if err != nil {
		log.Fatal("dialing:", err)
	}

	var request = new(JobRequest)
	var response *JobResponse
	var dummy = 0

	for {
		err = client.Call("PiRpc.RequestJob", dummy, request)
		if err != nil {
			log.Fatal("PiRpc error:", err)
		}

		fmt.Printf("Processing #%d\n", request.Id)

		response = NewJobResponse(*request)
		for d := request.From; d < request.To; d++ {
			response.Add(byte(piDigit(d)))
		}

		err = client.Call("PiRpc.PublishResults", response, &dummy)
		if err != nil {
			log.Fatal("PiRpc error:", err)
		}

		fmt.Printf("Processed #%d\n", request.Id)
	}
}

func getPiInfo() {
	client, err := rpc.DialHTTP("tcp", fmt.Sprintf("%s:%d", host, port))
	if err != nil {
		log.Fatal("dialing:", err)
	}

	var dummy int
	var pi string

	err = client.Call("PiRpc.Pi", &dummy, &pi)
	if err != nil {
		log.Fatal("PiRpc error:", err)
	}

	fmt.Println(pi)
}

func main() {
	flag.Usage = usage
	flag.Parse()

	if serverMode.IsServer() {
		startServer()
	} else if serverMode.IsClient() {
		startClient()
	} else if serverMode.IsPi() {
		getPiInfo()
	}
}
