/*
*	http://www.reddit.com/r/dailyprogrammer/comments/1qply1/111513_challenge_129_hard_baking_pi/
 */
package main

import (
	"math"
)

// http://en.wikipedia.org/wiki/Modular_exponentiation#Right-to-left_binary_method
func powMod(base float64, exp int, mod float64) float64 {
	res := 1.0
	base = math.Mod(base, mod)
	for exp > 0 {
		if exp%2 == 1 {
			res = math.Mod(res*base, mod)
		}
		exp = exp >> 1
		base = math.Mod(base*base, mod)
	}
	return res
}

func piTerm(n int, p float64) float64 {
	var s, nf, kf float64
	nf = float64(n)
	for k := 0; k < n; k++ {
		kf = float64(k)
		r := 8*kf + p
		s += powMod(16, n-k, r) / r
	}

	inf := 1.0
	for k := n; inf > math.SmallestNonzeroFloat64; k++ {
		kf = float64(k)
		inf = math.Pow(16, nf-kf) / (8*kf + p)
		s += inf
	}

	return s
}

func piDigit(n int) int {
	s := 4*piTerm(n, 1) - 2*piTerm(n, 4) - piTerm(n, 5) - piTerm(n, 6)
	s = s - math.Floor(s)

	return int(s * 16)
}
